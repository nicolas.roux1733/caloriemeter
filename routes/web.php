<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/repas', 'RepasController@index')->name('accueil');
Route::post('/repas/pardate', 'RepasController@repasParDate')->name('repas_pardate');
Route::get('/repas/view/{id}', 'RepasController@view')->name('repas_view');
Route::post('/repas/add', 'RepasController@postAdd')->name('repas_add');
Route::get('/repas/edit/{id}', 'RepasController@getEdit')->name('repas_edit');
Route::post('/repas/edit/{id}', 'RepasController@postEdit')->name('repas_edit');
Route::get('/repas/delete/{id}', 'RepasController@delete')->name('repas_delete');

Route::get('/produit/view/{id}', 'ProduitController@view')->name('produits_view');
Route::post('/produit/add/{id}', 'ProduitController@postAdd')->name('produits_add');
Route::get('/produit/delete/{id}', 'ProduitController@delete')->name('produits_delete');

Route::get('/statistiques', 'StatsController@index')->name('statistiques');




Route::get('/series/liste', 'SeriesController@index')->name('series_liste');
Route::get('/series/view/{id}', 'SeriesController@view')->name('series_view');
Route::get('/series/add', 'SeriesController@getAdd')->name('series_add');
Route::post('/series/add', 'SeriesController@postAdd')->name('series_add');
Route::get('/series/edit/{id}', 'SeriesController@getEdit')->name('series_edit');
Route::post('/series/edit/{id}', 'SeriesController@postEdit')->name('series_edit');
Route::get('/series/delete/{id}', 'SeriesController@delete')->name('series_delete');

Route::get('/saisons/add/{id}', 'SaisonsController@getAdd')->name('saisons_getAdd');
Route::post('/saisons/add', 'SaisonsController@postAdd')->name('saisons_add');
Route::get('/saisons/edit/{id}', 'SaisonsController@getEdit')->name('saisons_edit');
Route::post('/saisons/edit/{id}', 'SaisonsController@postEdit')->name('saisons_edit');
Route::get('/saisons/delete/{id}', 'SaisonsController@delete')->name('saisons_delete');