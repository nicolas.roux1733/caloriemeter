-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 05 mai 2019 à 19:21
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `caloriemeter`
--

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_03_05_164019_create_series_table', 1),
(2, '2019_03_05_175442_add_realisateur_to_series', 2),
(3, '2019_03_05_175541_delete_realisateur_to_series', 3),
(4, '2019_03_05_175649_add_realisateur_to_series', 4),
(5, '2019_03_05_175824_add_titre_to_series', 5),
(6, '2019_03_05_180425_add_realisateur_to_series', 6),
(7, '2019_03_06_140045_create_saisons_table', 7),
(8, '2019_03_06_140242_add_numero_to_saisons', 8),
(9, '2019_03_06_204841_create_episodes_table', 9),
(10, '2019_03_06_205621_add_numero_to_episodes', 10),
(11, '2019_03_06_205642_add_nom_to_episodes', 11),
(12, '2019_04_02_125952_create_repas_table', 12),
(13, '2019_04_02_130233_add_type_to_repas', 13),
(14, '2019_04_02_130500_add_date_to_repas', 14),
(15, '2019_04_02_130835_add_userid_to_repas', 15),
(16, '2019_04_02_214310_create_produits_table', 16),
(17, '2019_04_02_214712_create_repas_produits_table', 17);

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

DROP TABLE IF EXISTS `produits`;
CREATE TABLE IF NOT EXISTS `produits` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `marque` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_barre` bigint(20) NOT NULL,
  `kcal_100g` float NOT NULL,
  `mt_grasses` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mt_grasses_saturees` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `glucides` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sucre_100g` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `protein_100g` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sel_100g` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `produits`
--

INSERT INTO `produits` (`id`, `nom`, `marque`, `image_url`, `code_barre`, `kcal_100g`, `mt_grasses`, `mt_grasses_saturees`, `glucides`, `sucre_100g`, `protein_100g`, `sel_100g`, `created_at`, `updated_at`) VALUES
(70, '6 Merguez véritables', 'l-etal-du-boucher', 'https://static.openfoodfacts.org/images/products/20061210/front_fr.8.400.jpg', 20061210, 311, '28', '13.1', '2', '0.6', '13', '1.5', '2019-05-02 11:41:01', '2019-05-02 11:41:01'),
(71, 'Blanc de poulet BIO, -25% sel* - 2 tranches', 'fleury-michon', 'https://static.openfoodfacts.org/images/products/309/575/949/7017/front_fr.34.400.jpg', 3095759497017, 109, '1.5', '0.4', '0.6', '0.6', '23', '1.4', '2019-05-02 11:41:52', '2019-05-02 11:41:52'),
(72, 'Jamon Serrano', 'bonneterre', 'https://static.openfoodfacts.org/images/products/339/641/004/1328/front_fr.17.400.jpg', 3396410041328, 320, '20', '6', '0.5', '0.5', '35', '4.2', '2019-05-02 11:42:03', '2019-05-02 11:42:03'),
(73, 'Mousse de foie d\'oie truffée 1 %', 'castaing', 'https://static.openfoodfacts.org/images/products/333/227/000/0785/front_fr.7.400.jpg', 3332270000785, 0, '0', '0', '0', '0', '0', '0', '2019-05-02 11:42:14', '2019-05-02 11:42:14'),
(74, 'Céréales Bio Muesli Chocolat-Good Gout Kidz-300g', 'good-gout', 'https://static.openfoodfacts.org/images/products/376/026/931/0544/front_fr.4.400.jpg', 3760269310544, 441, '15', '3.2', '64', '18', '9.7', '0.02', '2019-05-02 11:42:54', '2019-05-02 11:42:54'),
(75, 'Joghurt mild 3.5%', 'milbona', 'https://static.openfoodfacts.org/images/products/20149802/front_fr.16.400.jpg', 20149802, 70, '3.5', '2.4', '4.9', '4.9', '4.1', '0.14', '2019-05-02 11:43:15', '2019-05-02 11:43:15'),
(76, 'Moules de Bouchot de la Baie du Mont Saint Michel', 'la-cancalaise', 'https://static.openfoodfacts.org/images/products/300/012/129/7768/front_fr.5.400.jpg', 3000121297768, 118, '2.8', '0.85', '3.1', '0.3', '20.2', '0.29', '2019-05-02 11:43:32', '2019-05-02 11:43:32'),
(77, 'Glace nougat fruits rouge', 'picard', 'https://static.openfoodfacts.org/images/products/327/016/073/0261/front_fr.8.400.jpg', 3270160730261, 235, '14', '8.5', '22', '20', '3.4', '0.1', '2019-05-02 11:44:19', '2019-05-02 11:44:19'),
(78, 'Chili con carne & riz', 'fleury-michon', 'https://static.openfoodfacts.org/images/products/330/274/048/4100/front_fr.75.400.jpg', 3302740484100, 104, '3.1', '1', '15', '1.9', '5.6', '0.79', '2019-05-02 11:44:41', '2019-05-02 11:44:41');

-- --------------------------------------------------------

--
-- Structure de la table `repas`
--

DROP TABLE IF EXISTS `repas`;
CREATE TABLE IF NOT EXISTS `repas` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `repas`
--

INSERT INTO `repas` (`id`, `created_at`, `updated_at`, `type`, `date`, `user_id`) VALUES
(51, '2019-05-02 11:44:07', '2019-05-02 11:44:07', 'dazd', '2019-05-02', 1),
(50, '2019-05-02 11:43:05', '2019-05-02 11:43:05', 'ae', '2019-05-02', 1),
(49, '2019-05-02 11:42:23', '2019-05-02 11:42:23', 'testb', '2019-05-02', 1),
(48, '2019-05-02 11:41:40', '2019-05-02 11:41:40', 'Déjeuner', '2019-05-02', 1),
(47, '2019-05-02 11:40:54', '2019-05-02 11:40:54', 'test', '2019-05-02', 1);

-- --------------------------------------------------------

--
-- Structure de la table `repas_produits`
--

DROP TABLE IF EXISTS `repas_produits`;
CREATE TABLE IF NOT EXISTS `repas_produits` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `repas_id` int(11) NOT NULL,
  `produit_id` int(11) NOT NULL,
  `qtt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kcal` float NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=95 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `repas_produits`
--

INSERT INTO `repas_produits` (`id`, `repas_id`, `produit_id`, `qtt`, `kcal`, `created_at`, `updated_at`) VALUES
(83, 47, 70, '100', 311, '2019-05-02 11:41:01', '2019-05-02 11:41:01'),
(84, 47, 70, '50', 156, '2019-05-02 11:41:21', '2019-05-02 11:41:21'),
(85, 48, 71, '100', 109, '2019-05-02 11:41:52', '2019-05-02 11:41:52'),
(86, 48, 72, '121', 387, '2019-05-02 11:42:03', '2019-05-02 11:42:03'),
(87, 48, 73, '122', 0, '2019-05-02 11:42:14', '2019-05-02 11:42:14'),
(88, 49, 72, '312', 998, '2019-05-02 11:42:32', '2019-05-02 11:42:32'),
(89, 49, 74, '124', 547, '2019-05-02 11:42:54', '2019-05-02 11:42:54'),
(90, 50, 75, '213', 149, '2019-05-02 11:43:15', '2019-05-02 11:43:15'),
(91, 50, 72, '87', 278, '2019-05-02 11:43:24', '2019-05-02 11:43:24'),
(92, 50, 76, '123', 145, '2019-05-02 11:43:32', '2019-05-02 11:43:32'),
(93, 51, 77, '121', 284, '2019-05-02 11:44:19', '2019-05-02 11:44:19'),
(94, 51, 78, '321', 334, '2019-05-02 11:44:41', '2019-05-02 11:44:41');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'nroux', 'nicolas.roux1733@gmail.com', NULL, '$2y$10$Y7ockf64BuUfmIQZeZ5M3uJagSHQuJ4XFVG40Z1rIPZkzSBXLZ7oO', NULL, '2019-04-06 07:51:24', '2019-04-06 07:51:24'),
(2, 'Jean Eude', 'niko.17@gmail.com', NULL, '$2y$10$Aihk2BE17J/7OrM788uTZuHKuYFCxaAzZ9TymDoMJ/AmMTsRPV9kq', NULL, '2019-04-02 10:32:28', '2019-04-02 10:32:28'),
(3, 'nrouxbis', 'niko.17@live.fr', NULL, '$2y$10$qCP89uT5Z7egNKNjI3RPBOD1kQ5QeWeu1M/pkcadk9PQ41d0dj0km', NULL, '2019-04-05 13:22:57', '2019-04-05 13:22:57');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
