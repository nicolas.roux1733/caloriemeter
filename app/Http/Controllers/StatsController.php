<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repas;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;



class StatsController extends Controller
{
    public function index()
    {
        if( \Auth::user() === null)
        {
            return view('auth.login');
        }
       
        $userId =  \Auth::user()->id;
        
        $top5max = DB::table('produits')
        ->leftjoin('repas_produits', 'repas_produits.produit_id', '=', 'produits.id')
        ->join('repas', 'repas.id', '=', 'repas_produits.repas_id')
        ->selectRaw('DISTINCT produits.kcal_100g, produits.nom')
        ->where('user_id',\Auth::user()->id)
        ->orderByRaw('kcal_100g desc')
        ->limit(5)
        ->get();

        $top5min = DB::table('produits')
        ->leftjoin('repas_produits', 'repas_produits.produit_id', '=', 'produits.id')
        ->join('repas', 'repas.id', '=', 'repas_produits.repas_id')
        ->selectRaw('DISTINCT produits.kcal_100g, produits.nom')
        ->where('user_id',\Auth::user()->id)
        ->orderByRaw('kcal_100g asc')
        ->limit(5)
        ->get();

        $top5allTime = DB::table('repas_produits')
        ->leftjoin('produits', 'repas_produits.produit_id', '=', 'produits.id')
        ->join('repas', 'repas.id', '=', 'repas_produits.repas_id')
        ->selectRaw('DISTINCT produits.nom, produits.id, repas_produits.kcal')
        ->where('user_id',\Auth::user()->id)
        ->get();

        $top5all = array();
        foreach($top5allTime as $key)
        {
            $top5all[$key->id][0] = isset($top5all[$key->id]) ? $top5all[$key->id][0] + $key->kcal :  $key->kcal;
            $top5all[$key->id][1] = $key->nom;            
        }

        arsort($top5all);


        return view('stats.index', ['top5max' => $top5max, 'top5min' => $top5min, 'top5all' => $top5all]);
    }

}
