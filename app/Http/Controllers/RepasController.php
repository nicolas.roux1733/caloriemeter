<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\DB;
use App\Repas;
use Illuminate\Support\Facades\Session;


class RepasController extends Controller
{
    public function index()
    {
        if( \Auth::user() === null)
        {
            return view('auth.login');
        }
       
        $userId =  \Auth::user()->id;
        $date = date('Y-m-d');
        $repas = Repas::where('user_id', $userId)
        ->where('date', $date)
        ->get();

        $kcalDay = 0;
        $kcalTable = [];
        
        foreach($repas as $repa)
        {
            
            $kcalTable[$repa->id] = 0;
            $produits = $repa->repasProduits;
            foreach($produits as $produit)
            {
                $kcalTable[$repa->id] = $kcalTable[$repa->id] + $produit->kcal;
                $kcalDay = $kcalDay + $produit->kcal;
            }
        }

        return view('repas.index', ['repasListe' => $repas, 'date' => $date, 'kcalListe' => $kcalTable, 'kcalDay' => $kcalDay]);
    }

    public function repasParDate(Request $request)
    {
        $userId =  \Auth::user()->id;
        $date = $request->input('date');
        $repas = Repas::where('user_id', $userId)
        ->where('date', $date)
        ->get();
        $kcalDay = 0;
        $kcalTable = [];
        
        foreach($repas as $repa)
        {
            
            $kcalTable[$repa->id] = 0;
            $produits = $repa->repasProduits;
            foreach($produits as $produit)
            {
                $kcalTable[$repa->id] = $kcalTable[$repa->id] + $produit->kcal;
                $kcalDay = $kcalDay + $produit->kcal;
            }
        }
        return view('repas.index', ['repasListe' => $repas, 'date' => $date, 'kcalListe' => $kcalTable, 'kcalDay' => $kcalDay]);
    }
    
    public function view($id)
    {
        $repas = Repas::find($id);
        $kcalDay = 0;
        $kcalTable = [];
        
        $kcalTable[$repas->id] = 0;
        $produits = $repas->repasProduits;
        foreach($produits as $produit)
        {
            $kcalTable[$repas->id] = $kcalTable[$repas->id] + $produit->kcal;
            $kcalDay = $kcalDay + $produit->kcal;
        }
        
        return view('repas.view', ['repas' => $repas, 'kcalListe' => $kcalTable, 'kcalDay' => $kcalDay]);
    }
 

    public function postAdd(Request $request)
    {
        $repas = new Repas();
        $repas->type = $request->input('type');
        $repas->date = $request->input('date');
        $repas->user_id = \Auth::user()->id;
        
        $repas->save();
        Session::flash('message_success', 'Le repas "'.$repas->type.'" à bien été ajouté à la date du '.$repas->date.' !'); 
        return redirect()->route('accueil');
    }

    public function getEdit($id)
    {
        $repas = Repas::find($id);
        return view('repas.edit', ['repas' => $repas, 'url' => '/repas/edit/'.$repas->id]);
    }

    public function postEdit($id, Request $request)
    {
        $repas = Repas::find($id);
        $repas->type = $request->input('type');
        $repas->date = $request->input('date');
        $repas->save();
        Session::flash('message_success', 'Le repas "'.$repas->type.'" du '.$repas->date.' série à bien été modifié !'); 
        return redirect()->route('accueil');
    }

    public function delete($id)
    {
        $repas = Repas::find($id);
        $repas->delete();
        Session::flash('message_alerte', 'Le repas "'.$repas->type.'" du '.$repas->date.' à bien été supprimé !');
        return redirect()->route('accueil');
    }
}
