<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\DB;
use App\RepasProduits;
use App\Produits;
use Illuminate\Support\Facades\Session;

class ProduitController extends Controller
{


    public function view($id)
    {
        $produit = Produits::find($id);
        return view('produits.view', ['produit' => $produit]);
    }

    public function getAdd($id)
    {
        return view('produits.add', ['id' => $id]);
    }

    public function postAdd($id, Request $request)
    {
        $produit = Produits::where('code_barre', $request->input('code_barre'))
        ->first();
        if($produit === null)
        {
            
            $url = 'https://fr.openfoodfacts.org/api/v0/produit/'.$request->input('code_barre').'.json';
            $data = json_decode(file_get_contents($url), true);
            if($data['status_verbose'] === 'product found')
            {
                $produit = new Produits();
                $produit->code_barre = $request->input('code_barre');
                $produit->nom = $data['product']['product_name'];

                $produit->marque = $data['product']['brands_tags'][0];
                $kcal = isset($data['product']['nutriments']['energy_100g']) ? $data['product']['nutriments']['energy_100g'] / 4.184 : 0;;
                $produit->kcal_100g = round($kcal);
                $produit->mt_grasses = isset($data['product']['nutriments']['fat_value']) ? $data['product']['nutriments']['fat_value']: 0;
                $produit->mt_grasses_saturees = isset($data['product']['nutriments']['saturated-fat_value']) ? $data['product']['nutriments']['saturated-fat_value']: 0;
                $produit->glucides = isset($data['product']['nutriments']['carbohydrates']) ? $data['product']['nutriments']['carbohydrates'] : 0;
                $produit->sucre_100g = isset($data['product']['nutriments']['sugars_100g']) ? $data['product']['nutriments']['sugars_100g'] : 0;
                $produit->protein_100g = isset($data['product']['nutriments']['proteins_100g']) ? $data['product']['nutriments']['proteins_100g'] : 0;
                $produit->sel_100g = isset($data['product']['nutriments']['salt_100g']) ? $data['product']['nutriments']['salt_100g'] : 0;



                $produit->image_url = $data['product']['image_front_url'];
                $produit->save();
            }

            else
            {
                //Si aucuns résultats ==> redirection avec message erreur
            }
        }
        
        $repasProduits = new RepasProduits();
        $repasProduits->repas_id = $id;
        $repasProduits->produit_id = $produit->id;
        $repasProduits->qtt = $request->input('qtt');
        
        $kcal = round($produit->kcal_100g / (100 / $request->input('qtt')));
        $repasProduits->kcal = $kcal;
        $repasProduits->save();
        Session::flash('message_success', 'Le produit à bien été ajouté !'); 
        return redirect()->route('repas_view', ['id' => $id]);
    }


    public function delete($id)
    {
        $repasProduits = RepasProduits::find($id);
        $repasProduits->delete();
        Session::flash('message_alerte', 'Le produit à bien été supprimé !');
        return redirect()->route('repas_view', ['id' => $repasProduits->repas_id]);
    }
}
