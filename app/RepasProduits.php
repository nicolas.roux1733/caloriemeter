<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RepasProduits extends Model
{
    public function repas()
    {
        return $this->belongsTo(Repas::class);
    }

    public function produit()
    {
        return $this->belongsTo(Produits::class);
    }
}