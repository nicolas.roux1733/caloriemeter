<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produits extends Model
{
    public function repasProduits()
    {
        return $this->hasMany(RepasProduits::class)->orderBy('id');
    }
}
