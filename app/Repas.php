<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Repas extends Model
{
    public function repasProduits()
    {
        return $this->hasMany(RepasProduits::class)->orderBy('id');
    }

    public function getCalories()
        {
            $kcal = 0;
            foreach($this->hasMany(RepasProduits::class) as $produit)
            {
                $kcal = $kcal + $produit->kcal;
            }
            return $kcal;
        }
}
