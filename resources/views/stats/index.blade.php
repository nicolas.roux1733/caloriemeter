@extends('base')
@section('contenu')
@extends('flash_message')

    <div class="row bd-bt">
        <div class="col-md-3" align="left"></div>
        <div class="col-md-6" align="center"><h4>Statistiques</h4></div>
        <div class="col-md-3" align="right"></div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="bloc-repas col-md-8 mts">
            <h4 align="center" class="repas-title">Produits consommés les <b>plus</b> élevés énergétiquement</h4>
        
            <table class="table">
                <tr>
                    <th>Nom du produit</th>
                    <th>Kcal 100g</th>
                </tr>

                @foreach ($top5max as $max)
                    <tr>
                        <td>{{ $max->nom }}</td>
                        <td>{{ $max->kcal_100g }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="col-md-2"></div>
    </div>

    <br><br>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="bloc-repas col-md-8 mts">
            <h4 align="center" class="repas-title">Produits consommés les <b>moins</b> élevés énergétiquement</h4>
        
            <table class="table">
                <tr>
                    <th>Nom du produit</th>
                    <th>Kcal 100g</th>
                </tr>
                @foreach ($top5min as $min)
                    <tr>
                        <td>{{ $min->nom }}</td>
                        <td>{{ $min->kcal_100g }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="col-md-2"></div>
    </div>

    <br><br>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="bloc-repas col-md-8 mts">
            <h4 align="center" class="repas-title">Produits dont la totalité consommée sont les plus élevés énergétiquement</h4>
        
            <table class="table">
                <tr>
                    <th>Nom du produit</th>
                    <th>Kcal totale consommée</th>
                </tr>
                @foreach ($top5all as $all)
                    <tr>
                        <td>{{ $all[1] }}</td>
                        <td>{{ $all[0] }}</td>
                    </tr>
                    @if ($loop->index == 4)
                        @break
                    @endif
                @endforeach
            </table>
        </div>
        <div class="col-md-2"></div>
    </div>
@endsection