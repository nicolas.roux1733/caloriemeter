@extends('base')
@section('contenu')
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<br><h1>Ajout d'un repas</h1><br>

            {!! Form::open(['url' => '/repas/add']) !!}
                <div class="form-group">
                    Type :
                    {!! Form::text('type', '') !!}
                    <br>
                </div>
                <div class="form-group">
                    Date :
                    {!! Form::date('date', 'Date : ') !!}
                    <br>
                </div>
                {!! Form::submit('Envoyer !') !!}
            {!! Form::close() !!}
		</div>
		<div class="col-md-3"></div>
	</div>
@endsection
