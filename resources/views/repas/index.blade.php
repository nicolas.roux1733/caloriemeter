@extends('base')
@section('contenu')
@extends('flash_message')

    <div class="row bd-bt">
        <div class="col-md-3" align="left">
            <a href="#"> <h2><a href="#"><i class='fas fa-angle-left' style='font-size:40px;color:#007BFF'></i></a></h2> </a>
        </div>
        <div class="col-md-6" align="center"><h4>Repas du {{$date}} : {{$kcalDay}} Calories</h4></div>
        <div class="col-md-3" align="right">
            <a href="#"> <h2><i class='fas fa-angle-right' style='font-size:40px;color:#007BFF'></i></h2> </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <br>
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal">Ajouter un repas</button><br><br>
            <a href="{{ route('statistiques') }}"><button type="button" class="btn btn-info">Statistiques</button></a><br><hr>
            {!! Form::open(['url' => '/repas/pardate']) !!}
                Choisir une autre date :
                <div class="form-group">
                    {!! Form::date('date', '') !!}
                    <br>
                </div>
                {!! Form::submit('Actualiser') !!}
            {!! Form::close() !!}
            <hr>
            <div class="alert alert-info">
                <strong>Calories :</strong> Les calories sont exprimées en Kilos Calories (KCal).<br><br>
                <strong>N/I :</strong> Non Indiqué : Les données caloriques ne sont pas précisées sur la fiche produit.
            </div>

        </div>
        <div class="col-md-9 repas-liste">
            <br>
            @foreach ($repasListe as $repas)
                <div class="bloc-repas">
                    <div class="row">
                        <div class="col-md-6 mts">
                            <h3 align="center" class="repas-title">{{ $repas->type }} : {{ $kcalListe[$repas->id] }} Calories</h3>
                        </div>
                        <div class="col-md-2 mts">
                            <a href="{{ route('repas_view', [$repas->id]) }}"><i class='far fa-eye icon icon-view'></i></a>
                        </div>
                        <div class="col-md-2 mts">
                            <a href="{{ route('repas_edit', [$repas->id]) }}"><i class='far fa-edit icon icon-edit'></i></a>
                        </div>
                        <div class="col-md-2 mts">
                            <a href="{{ route('repas_delete', [$repas->id]) }}"><i class='far fa-trash-alt icon icon-delete'></i></a>
                        </div>
                    </div>

                    <table class="table">
                        @foreach ($repas->repasProduits as $repasProduit)
                        <tr>
                            <td>
                                <a href="{{ route('produits_view', [$repasProduit->produit->id]) }}">
                                    {{ $repasProduit->produit->nom }}
                                </a>
                            </td>
                            <td>
                                {{ $repasProduit->qtt }}g
                            </td>
                            <td>
                                @if ($repasProduit->kcal == 0) 
                                    N/I*
                                @else
                                    {{ $repasProduit->kcal}}  kcal
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('produits_delete', [$repasProduit->id]) }}"><i class='far fa-trash-alt' style='font-size:20px;color:#007BFF'></i></a>
                            </td>
                        </tr>
                            
                        @endforeach
                        <tr>
                                <td><a href="{{ route('repas_view', [$repas->id]) }}">Rendez vous sur la fiche repas pour ajouter un produit</a></td><td></td><td></td><td></td>
                        </tr>
                    </table>
                        
                </div>
                <br><br>
                @php
                    $urlAddProduct = '/produit/add/'.$repas->id
                @endphp
            @endforeach
        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title">Ajout d'un repas</h4>
                </div>
                <div class="modal-body">
                {!! Form::open(['url' => '/repas/add']) !!}
                    <div class="form-group">
                        Type :
                        {!! Form::text('type', '') !!}
                        <br>
                    </div>
                    <div class="form-group">
                        Date :
                        {!! Form::date('date', 'Date : ') !!}
                        <br>
                    </div>
                    {!! Form::submit('Envoyer !') !!}
                {!! Form::close() !!}   
                </div>
                <div class="modal-footer">
                
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
@endsection