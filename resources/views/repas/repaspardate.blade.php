@extends('base')
@section('contenu')
    @extends('flash_message')
    <div class="row bd-bt">
        <div class="col-md-4">
        </div>
        <div class="col-md-6" align="center"><h4>{{ $repas->date }} - {{ $repas->type }} : {{ $kcalListe[$repas->id] }} Calories</h4></div>
        <div class="col-md-2">
        </div>
    </div><br>
    <div class="row">
        <div class="col-md-3">
            <a href="{{ route('accueil') }}"><button type="button" class="btn btn-primary btn-sm">Retour à l'accueil</button></a>
            <br><hr>
            <div class="alert alert-info">
                <strong>Calories :</strong> Les calories sont exprimées en Kilos Calories (KCal).<br><br>
                <strong>N/I :</strong> Non Indiqué : Les données caloriques ne sont pas précisées sur la fiche produit.
            </div>
        </div>
        <div class="col-md-9 br bl">
            <div class="bloc-repas">
                <div class="row">
                    <div class="col-md-8 mts">
                        <h3 align="center" class="repas-title">{{ $repas->type }} : {{ $kcalListe[$repas->id] }} Calories</h3>
                    </div>
                    <div class="col-md-2 mts">
                        <a href="{{ route('repas_edit', [$repas->id]) }}"><i class='far fa-edit icon icon-edit'></i></a>
                    </div>
                    <div class="col-md-2 mts">
                        <a href="{{ route('repas_delete', [$repas->id]) }}"><i class='far fa-trash-alt icon icon-delete'></i></a>
                    </div>
                </div>

                <table class="table">
                    @foreach ($repas->repasProduits as $repasProduit)
                        <tr>
                            <td>
                                <a href="{{ route('produits_view', [$repasProduit->produit->id]) }}">
                                    {{ $repasProduit->produit->nom }}
                                </a>
                            </td>
                            <td>
                                {{ $repasProduit->qtt }}g
                            </td>
                            <td>
                                @if ($repasProduit->kcal == 0)
                                    N/I*
                                @else
                                    {{ $repasProduit->kcal}}  kcal
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('produits_delete', [$repasProduit->id]) }}"><i class='far fa-trash-alt' style='font-size:20px;color:#007BFF'></i></a>
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td><button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">Ajouter un produit</button></td><td></td><td></td><td></td><td></td>
                    </tr>
                </table>

            </div>
            @php
                $urlAddProduct = '/produit/add/'.$repas->id
            @endphp
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Ajout d'un produit</h4>
                        </div>
                        <div class="modal-body row">
                            <div class="col-md-6 br">
                                {!! Form::open(['url' => $urlAddProduct]) !!}
                                <div class="form-group">
                                    Code du produit à ajouter :<br>
                                    {!! Form::text('code_barre', '') !!}
                                    <br>
                                </div>
                                <div class="form-group">
                                    Quantitée en grammes :<br>
                                    {!! Form::text('qtt', '') !!}
                                    <br>
                                </div>
                                {!! Form::submit('Ajouter') !!}
                                {!! Form::close() !!}
                            </div>
                            <div class="col-md-6">
                                <div class="alert alert-info">
                                    <strong>Info :</strong> Les produits sont liés à une API, pour voir la liste des produits disponibles, rendez vous sur <a href="https://fr.openfoodfacts.org" target="_blank">openfoodfacts.org</a>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">

                            <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection