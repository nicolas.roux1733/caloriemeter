@extends('base')
@section('contenu')
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
            <br><h1>Edition du repas {{$repas->type}} du {{$repas->date}}</h1><br>        
            {!! Form::open(['url' => $url]) !!}
                <div class="form-group">
                    Type :
                    {!! Form::text('type', '') !!}
                    <br>
                </div>
                <div class="form-group">
                    Date :
                    {!! Form::date('date', '') !!}
                    <br>
                </div>
                {!! Form::submit('Mettre à jour !') !!}
            {!! Form::close() !!}
		</div>
		<div class="col-md-3"></div>
	</div>
@endsection
