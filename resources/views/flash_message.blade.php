<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
@if(Session::has('message_alerte'))
    <p style="text-align:center" class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('message_alerte') }}</p>
@endif
@if(Session::has('message_success'))
    <p style="text-align:center" class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message_success') }}</p>
@endif