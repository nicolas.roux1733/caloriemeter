@extends('base')
@section('contenu')
    @extends('flash_message')
    <div class="row bd-bt">
        <div class="col-md-4">
        </div>
        <div class="col-md-6" align="center"><h4>Fiche produit : {{ $produit->nom }}</h4></div>
        <div class="col-md-2">
        </div>
    </div><br>
    <div class="row">
        <div class="col-md-3">
            <a href="{{ route('accueil') }}"><button type="button" class="btn btn-primary btn-sm">Retour à l'accueil</button></a>
            <br><hr>
            <div class="alert alert-info">
                <strong>Calories :</strong> Les calories sont exprimées en Kilos Calories (KCal).<br><br>
                <strong>N/I :</strong> Non Indiqué : Les données caloriques ne sont pas précisées sur la fiche produit.
            </div>
        </div>
        <div class="col-md-9 br bl">
            <div align="center">
                <h3>Photo du produit</h3><br>
                <img src="{{ $produit->image_url }}"/>
            </div>
            </br><hr>

            <h3>Informations générales</h3>
            <b>Code barre : </b>{{ $produit->code_barre }}</br>
            <b>Nom : </b>{{ $produit->nom }}</br>
            <b>Marque : </b>{{ $produit->marque }}</br><hr>

            <h3>Informations nutritionnelles pour 100 grammes</h3>
            <b>Énergie : </b>{{ number_format($produit->kcal_100g ,2) }} Kcal</br>
            <b>Matières grasses : </b>{{ $produit->mt_grasses }} g</br>
            <b>Matères grasses saturées : </b>{{ $produit->mt_grasses_saturees }} g</br>
            <b>Glucides : </b>{{ $produit->glucides }} g</br>
            <b>Sucres : </b>{{ $produit->sucre_100g }} g</br>
            <b>Protéines : </b>{{ $produit->protein_100g }} g</br>
            <b>Sel : </b>{{ $produit->sel_100g }} g</br>
        </div>
    </div>
@endsection