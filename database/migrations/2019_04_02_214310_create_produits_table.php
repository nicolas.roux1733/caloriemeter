<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProduitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom');
            $table->string('marque');
            $table->string('image_url');
            $table->bigIncrements('code_barre');
            $table->floatval('kcal_100g');
            $table->string('mt_grasses');
            $table->string('mt_grasses_saturees');
            $table->string('glucides');
            $table->string('sucre_100g');
            $table->string('protein_100g');
            $table->string('sel_100g');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produits');
    }
}
